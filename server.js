// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const mysql = require('mysql');

// Get our API routes
const index = require('./routes/index');
const api = require('./routes/api');

const app = express();

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// Point static path to dist
app.use(express.static(path.join(__dirname, 'client/dist')));

// Set our api routes
app.use('***', express.static(path.join(__dirname, 'client/dist')));
app.use('/', index);
app.use('/api', api);

// Catch all other routes and return the index file
/**app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'client/src/index.html'));
}); **/

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));

