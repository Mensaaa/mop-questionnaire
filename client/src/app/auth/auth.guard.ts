import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { DataService } from '../services/data.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private dataService: DataService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {
    if (this.dataService.currentUser == null) {
      this.router.navigate(['/login']);
      return false;
    }

    if(!this.dataService.currentUser.isAdmin && this.dataService.currentUser.isLogged){
      return true;
    }

    if (this.dataService.currentUser.isAdmin) {
      return true
    }
    else {
      this.router.navigate(['/login']);
      return false;
    }

  }
}
