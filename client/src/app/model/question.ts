export class Question{
    constructor(
        public id:number,
        public questionnaireId: number,
        public questionNum:number,
        public type: number,
        public text: string,
        public choice: string,
        public choices: string[]
    ){

    }
}