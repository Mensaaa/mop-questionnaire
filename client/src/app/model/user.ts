export class User {
    constructor
        (
        public id: number,
        public name: string,
        public lastName: string,
        public email: string,
        public password: string,
        public confirmPassword: string,
        public isAdmin: boolean,
        public isLogged: boolean
        ) { }
}