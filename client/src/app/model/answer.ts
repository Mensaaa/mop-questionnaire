export class Answer {
    constructor(
        public questionnaireId: number,
        public userId: number,
        public questionNum: number,
        public questionAnswer: string
    ) {

    }
}