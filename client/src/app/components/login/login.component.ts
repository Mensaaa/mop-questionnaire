import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { DataService } from '../../services/data.service';
import { GlobalEventsManagerService } from '../../services/global-events-manager.service';

import { User } from '../../model/user';

import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  errorMessage:String = "";
  isError: boolean = false;

  constructor(private router: Router, private dataService: DataService, private gEM: GlobalEventsManagerService ) { }

  ngOnInit() {
    this.configForm();
  }

  configForm() {
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.minLength(6))
    });
  }

  cancel() {
    this.configForm();
  }

  login(model: User) {
    this.isError = false;
    this.dataService.loginCheck(model.email, model.password)
      .subscribe(data => {
        if(data.type == 1) {
          this.errorMessage = data.msg;
          this.isError = true;
          return;
        }
        model.isAdmin = data.is_admin;
        model.lastName = data.lastName;
        model.name = data.name;
        model.id = data.id;
        model.isLogged = true;
        if (model.isAdmin) {
          this.setCurrentUser(model);
          this.router.navigate(['/admin-panel']);
          this.gEM.showNavBar(true);
          this.gEM.showBackNavBar(false);
          this.gEM.showName(model.name);
        } else {
          this.setCurrentUser(model);
          this.gEM.showNavBar(true);
          this.gEM.showBackNavBar(false);
          this.gEM.showName(model.name);
          this.router.navigate(['/home']);
        }
      }, err => {
        console.log(err);
      })

  }

  setCurrentUser(curUser: User) {
    this.dataService.currentUser = curUser;
  }

}
