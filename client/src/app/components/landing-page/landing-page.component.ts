import { Component, OnInit } from '@angular/core';

import { GlobalEventsManagerService } from '../../services/global-events-manager.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  constructor(private gEM: GlobalEventsManagerService) { }

  ngOnInit() {
  }

  addNavbar() {
    this.gEM.showBackNavBar(true);
  }

}
