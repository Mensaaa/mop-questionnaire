import { Component, OnInit, ViewContainerRef, ElementRef } from '@angular/core';

import { DataService } from '../../services/data.service';

import { Questionnaire } from '../../model/questionnaire';

import { Question } from '../../model/question';
import { Answer } from '../../model/answer';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

declare var jQuery: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  questionnaire: Questionnaire[];
  question: Question[];
  questionInsert: Question[];
  questionChoice: any = [];
  isError: boolean = false;
  constructor(private dataService: DataService, private toasts: ToastsManager, vcr: ViewContainerRef, private elRef: ElementRef) {
    this.toasts.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.questionnaire = new Array<Questionnaire>();
    this.getQuestionnairesForUsers();
  };

  getQuestionnairesForUsers() {
    let userId = this.dataService.currentUser.id;
    this.dataService.getQuestionnairesForUsers(userId)
      .subscribe(data => {
        this.questionnaire = data;
      });
  }


  getQuestions(id: number) {
    this.dataService.getQuestions(id)
      .subscribe(data => {
        this.question = data;
        this.question.forEach((el) => {
          if (el.choices.length > 0) {
            let ch = el.choices.toString();
            el.choices = ch.split(",");
          };
        });
        this.questionInsert = data;
      }, err => {
        console.log(err);
      });
  };

  save() {
    let userId = this.dataService.currentUser.id;
    let startNum = 0;
    let answers: any[] = new Array();
    this.isError = false;
    this.questionInsert.forEach(el => {
      let _el = Object.assign({}, el)
      if (_el["type"] == 3) {
          el.choice = this.questionChoice;
      }
      let answer = new Array(_el["questionnareId"],userId , _el["questionNum"], el.choice.toString());
      answers.push(answer);
    });

    for (let i = 0; i < answers.length; i++) {
      if (answers[i][3] == "" || answers[i][3] == null) {
        this.isError = true;
        return;
      };
    };
    this.dataService.saveUserAnswers(answers)
      .subscribe(data => {
        if (data.type == 0) {
          this.toasts.success("You have successfully completed this questionnaire!", 'Success');
        }
      }, err => {
        console.log(err);
      });
    jQuery(this.elRef.nativeElement).find('#questionModal').modal('toggle');
    this.getQuestionnairesForUsers();
  };

  close() {
    this.getQuestionnairesForUsers();
  }

}
