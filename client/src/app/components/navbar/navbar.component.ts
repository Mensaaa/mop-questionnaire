import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { GlobalEventsManagerService } from '../../services/global-events-manager.service';
import { DataService } from '../../services/data.service';

import { User } from '../../model/user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  showNavBar: boolean = false;
  showBackNavBar: boolean = false;
  user: string;

  constructor(private gEM: GlobalEventsManagerService, private DataService: DataService) {
    this.gEM.showNavBarEmitter.subscribe((mode) => {
      // mode will be null the first time it is created, so you need to igonore it when null
      if (mode !== null) {
        this.showNavBar = mode;
      }
    });
    this.gEM.showBackNavBarEmitter.subscribe((mode) => {
      if (mode !== null) {
        this.showBackNavBar = mode;
      }
    });
    this.gEM.showNameEmmiter.subscribe((mode) => {
      if (mode !== null) {
        this.user = mode;
      }
    });
  }

  ngOnInit() {
    
  }

  back() {
    this.gEM.showBackNavBar(false);
  }

  logout() {
    let resetUser = new User(1,"","","","","",false,false);
    this.gEM.showNavBar(false);
    this.DataService.currentUser = resetUser;
  }

}
