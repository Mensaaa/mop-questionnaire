import { Component, OnInit, Input, ViewContainerRef, ViewChild, ElementRef, Renderer } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { DataService } from '../../services/data.service';

import { Questionnaire } from '../../model/questionnaire';
import { Question } from '../../model/question';
import { Answer } from '../../model/answer';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  constructor(private dataService: DataService, private toasts: ToastsManager, vcr: ViewContainerRef, private renderer: Renderer) {
    this.toasts.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    //this.getQuestions();
    console.log(this.dataService.currentUser);
    this.questionChoice = new Array<Question>();
  }

  ngAfterViewInit() {

  }

  @ViewChild('closeModal') closeModal: ElementRef;
  @Input() questionnaire: Questionnaire;
  question: Question[];
  questionForm: FormGroup;
  questionInsert: Question[];
  questionChoice: any = [];
  isError: boolean = false;

  statusToggle() {

  }

  deleteTask() {

  }

  getQuestions(id: number) {
    this.question = [];
    this.questionInsert = [];
    console.log(this.question);
    console.log(this.questionInsert);
    console.log(id);
    this.dataService.getQuestions(id)
      .subscribe(data => {
        console.log(data);
        this.question = data;
        this.question.forEach((el) => {
          if (el.choices.length > 0) {
            let ch = el.choices.toString();
            el.choices = ch.split(",");
          };
        });
        this.questionInsert = data;
      }, err => {
        console.log(err);
      });
    console.log(this.question);
    console.log(this.questionInsert);
  };

  save() {
    console.log(this.questionInsert);
    console.log(this.questionChoice);
    let startNum = 0;
    let answers: any[] = new Array();
    this.isError = false;
    this.questionInsert.forEach(el => {
      let _el = Object.assign({}, el)
      if (_el["type"] == 3) {

      }
      let answer = new Array(_el["questionnareId"], 1, _el["questionNum"], el.choice);
      answers.push(answer);
    });

    for (let i = 0; i < answers.length; i++) {
      if (answers[i][3] == "" || answers[i][3] == null) {
        this.isError = true;
        return;
      };
    };
    this.dataService.saveUserAnswers(answers)
      .subscribe(data => {
        console.log(data);
        if (data.type == 0) {
          this.toasts.success("You have successfully completed this questionnaire!", 'Success');
        }
      }, err => {
        console.log(err);
      });
  };

  close() {
    this.question = [];
    this.questionInsert = [];
  }

}
