import { Component, OnInit, ElementRef } from '@angular/core';

import { DataService } from '../../services/data.service';

import { User } from '../../model/user';
import { Question } from '../../model/question';
import { Questionnaire } from '../../model/questionnaire';

declare var jQuery: any;

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {

  constructor(private dataService: DataService, private elRef: ElementRef) { }
  email: string;
  numQuestion: number = 1;
  question: Question;
  questions: Question[];
  questionnaire: Questionnaire;
  questionnaires: Questionnaire[];
  private isUpdate: boolean = false;
  private currUser: User;
  isError: boolean = false;
  lastNum: number = 0;

  ngOnInit() {
    this.getQuestionnaires();
    this.currUser = this.dataService.currentUser;
    this.question = new Question(1, 1, this.numQuestion, 1, "", "", []);
    this.questions = new Array<Question>();
    this.questionnaire = new Questionnaire(1, "");

  }

  getQuestionnaires() {
    this.dataService.getQuestionnaires()
      .subscribe(data => {
        this.questionnaires = data;
      }, err => {
        console.log(err);
      });
  };

  insertQuestion(q: Question) {
    if(this.lastNum != 0){
       q.questionNum = this.lastNum;
       this.lastNum++;
    };

    this.questions.push(q);
    this.numQuestion++;
    this.question = new Question(1, 1, this.numQuestion, 1, "", "", []); 
  }

  deleteQuestion(row: number) {
    row--;
    let newRow = 1;
    this.questions.splice(row, 1);
    for(let i = 0; i<this.questions.length; i++) {
      this.questions[i].questionNum = newRow;
      newRow++;
    }
    this.lastNum = newRow;
    (this.questions);
  };

  close() {
    this.getQuestionnaires();
    this.isUpdate = false;
    this.numQuestion = 1;
  }

  save() {
    if (this.isUpdate == false) {
      if(this.questions.length == 0 || this.questionnaire.name == "") {
        this.isError = true;
        return;
      };
      let lastId = 0;
      this.dataService.saveQuestionnaire(this.questionnaire)
        .subscribe(data => {
          lastId = data.lastId;
          for (let i = 0; i < this.questions.length; i++) {
            if(this.questions[i].choices.length == 0) {
              let empty ="";
              this.questions[i].choices.push(empty);
            }
            this.questions[i].questionnaireId = lastId;
          }
          this.dataService.saveQuestions(this.questions)
            .subscribe(data => {
            }, err => {
              console.log(err);
            });
          this.questions = [];
          this.getQuestionnaires();
        }, err => {
          console.log(err);
        });
    } else {
      this.dataService.updateQuestionnaire(this.questionnaire)
        .subscribe(data => {
          this.dataService.updateQuestions(this.questions)
            .subscribe(data => {
            });
        });
    };
    this.isUpdate = false;
    this.numQuestion = 1;
    this.isError = false;
    jQuery(this.elRef.nativeElement).find('#myModal').modal('toggle');
  };

  setCurrentQuestionnaire(q: Questionnaire, type: number) {
    this.questionnaire = q;
    if (type == 0) {
      this.isUpdate = true;
      this.dataService.getQuestions(this.questionnaire.id)
        .subscribe(data => {
          this.questions = data;
        })
    }
  }

  deleteQuestionnaire() {
    this.dataService.deleteQuestionnaire(this.questionnaire.id)
      .subscribe(data => {
      }, err => {
      });
    this.getQuestionnaires();
  };

  reset() {
    this.questions = new Array<Question>();
    this.questionnaire = new Questionnaire(1, "");
    this.isUpdate = false;
    this.numQuestion = 1;
  }

}
