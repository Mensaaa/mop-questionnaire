import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { DataService } from '../services/data.service';

import { User } from '../model/user';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  private confirmPassword: string;
  isError: boolean = false;
  errorMessage: string = "";

  constructor(private dataService: DataService,
    private toasts: ToastsManager, vcr: ViewContainerRef, private router: Router) {
    this.toasts.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.configureForm();
  }

  configureForm() {
    this.registerForm = new FormGroup({
      name: new FormControl("", [Validators.required]),
      last_name: new FormControl("", Validators.required),
      email: new FormControl("", Validators.required),
      password: new FormControl("", Validators.minLength(6)),
      confirmPassword: new FormControl(this.confirmPassword, Validators.minLength(6))
    })
  }

  register(model: User) {
    this.isError = false;
    if (model.password != model.confirmPassword) {
      this.isError = true;
      this.errorMessage = "Passwords don't match!";
      return;
    }

    this.dataService.registerUser(model)
      .subscribe(data => {
        if(data.type == 1) {
          this.isError = true;
          this.errorMessage = data.msg;
          return;
        }
        this.toasts.success("You are successfully registered!", 'Success');
        setTimeout(() => {
          this.router.navigate(['/login']);
        }, 2000);
      }, err => {
        console.log(err);
      })

  }

}
