import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Observable } from "rxjs/Observable"

@Injectable()
export class GlobalEventsManagerService {

  private _showNavBar: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  public showNavBarEmitter: Observable<boolean> = this._showNavBar.asObservable();

  private _showBackNavBar: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  public showBackNavBarEmitter: Observable<boolean> = this._showBackNavBar.asObservable();

  private _showName: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  public showNameEmmiter: Observable<string> = this._showName.asObservable();

  constructor() { }

  showNavBar(ifShow: boolean) {
    this._showNavBar.next(ifShow);
  }

  showBackNavBar(ifShow: boolean) {
    this._showBackNavBar.next(ifShow);
  }

  showName(name: string) {
    this._showName.next(name);
  }
}
