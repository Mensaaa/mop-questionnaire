import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, Jsonp } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { User } from '../model/user';
import { Questionnaire } from '../model/questionnaire';
import { Question } from '../model/question';

@Injectable()
export class DataService {

  constructor(private http: Http, private _jsonp: Jsonp) { }

  currentUser: User;

  isCurrentUserLogged(): boolean {
    return this.currentUser.isLogged;
  }

  loginCheck(email: string, password: string): Observable<any> {
    return this.http.post('api/user', { email: email, password: password })
      .map((res: Response) => {

        return res.json();
      })
      .catch(err => {
        return Observable.throw(err);
      })
  }

  registerUser(user: User): Observable<any> {
    return this.http.post('api/register', {
      user: user
    })
      .map((res: Response) => {
        return res.json();
      })
      .catch(err => {
        return Observable.throw(err);
      })
  }

  saveQuestionnaire(q: Questionnaire): Observable<any> {
    return this.http.post('/api/questionnaire', { data: q })
      .map((res: Response) => {
        return res.json();
      })
      .catch(err => {
        return Observable.throw(err);
      })
  };

  saveQuestions(q: Question[]): Observable<any> {;
    return this.http.post('/api/questions', q)
      .map((res: Response) => {

        return res.json();
      })
      .catch(err => {
        return Observable.throw(err);
      });
  };

  getQuestionnaires(): Observable<any> {
    return this.http.post('/api/questionnaires', [])
      .map((res: Response) => {

        return res.json();
      })
      .catch(err => {
        return Observable.throw(err);
      });
  };

  getQuestionnairesForUsers(id: number): Observable<any> {
    return this.http.post('/api/questionnairesUser', { data: id })
      .map((res: Response) => {

        return res.json();
      })
      .catch(err => {
        return Observable.throw(err);
      });
  };

  deleteQuestionnaire(id: number): Observable<any> {
    return this.http.post('api/deleteQuestionnaire', { data: id })
      .map((res: Response) => {

        return res.json();
      })
      .catch(err => {
        return Observable.throw(err);
      })
  }

  getQuestions(id: number): Observable<any> {
    return this.http.post('api/questionsFrom', { data: id })
      .map((res: Response) => {
        return res.json();
      })
      .catch(err => {
        return Observable.throw(err);
      });
  };

  saveUserAnswers(q: any): Observable<any> {
    return this.http.post('/api/useranswers', q)
      .map((res: Response) => {
        return res.json();
      })
      .catch(err => {
        return Observable.throw(err);
      })
  };

  updateQuestionnaire(q: any): Observable<any> {
    return this.http.post('api/updateQuestionnaire', q)
      .map((res: Response) => {
        return res.json();
      })
      .catch(err => {
        return Observable.throw(err);
      });
  };

  updateQuestions(q: any): Observable<any> {
    return this.http.post('api/updateQuestions', q)
      .map((res: Response) => {
        return res.json();
      })
      .catch(err => {
        return Observable.throw(err);
      });
  };

}
