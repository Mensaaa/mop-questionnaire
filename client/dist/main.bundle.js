webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"site-wrapper\">\n\n  <div class=\"site-wrapper-inner\">\n\n    <div class=\"cover-container\">\n\n      <div class=\"masthead clearfix\">\n        <div class=\"inner\">\n          <h3 class=\"masthead-brand\">MOP Questionnaire</h3>\n          <nav class=\"nav nav-masthead\">\n            <app-navbar></app-navbar>\n          </nav>\n        </div>\n      </div>\n      <div>\n        <router-outlet></router-outlet>\n      </div>\n      <footer class=\"col-md-12\">\n        <div class=\"mastfoot\">\n          <div class=\"inner\">\n            <p>Web app made by <a href=\"https://www.linkedin.com/in/mensur-bekti%C4%87-725a44135/\">Mensur Bektić</a></p>\n          </div>\n        </div>\n      </footer>\n    </div>\n\n  </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__ = __webpack_require__("../../../../ng2-toastr/ng2-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/@angular/platform-browser/animations.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__register_register_component__ = __webpack_require__("../../../../../src/app/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_landing_page_landing_page_component__ = __webpack_require__("../../../../../src/app/components/landing-page/landing-page.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_login_login_component__ = __webpack_require__("../../../../../src/app/components/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_admin_panel_admin_panel_component__ = __webpack_require__("../../../../../src/app/components/admin-panel/admin-panel.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_global_events_manager_service__ = __webpack_require__("../../../../../src/app/services/global-events-manager.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_card_card_component__ = __webpack_require__("../../../../../src/app/components/card/card.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_navbar_navbar_component__ = __webpack_require__("../../../../../src/app/components/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__auth_auth_guard__ = __webpack_require__("../../../../../src/app/auth/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_home_home_component__ = __webpack_require__("../../../../../src/app/components/home/home.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_9__components_landing_page_landing_page_component__["a" /* LandingPageComponent */] },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_8__register_register_component__["a" /* RegisterComponent */] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_10__components_login_login_component__["a" /* LoginComponent */] },
    { path: 'home', component: __WEBPACK_IMPORTED_MODULE_17__components_home_home_component__["a" /* HomeComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_16__auth_auth_guard__["a" /* AuthGuard */]] },
    {
        path: 'admin-panel',
        component: __WEBPACK_IMPORTED_MODULE_11__components_admin_panel_admin_panel_component__["a" /* AdminPanelComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_16__auth_auth_guard__["a" /* AuthGuard */]]
    },
    { path: '**', redirectTo: '/' }
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_8__register_register_component__["a" /* RegisterComponent */],
            __WEBPACK_IMPORTED_MODULE_9__components_landing_page_landing_page_component__["a" /* LandingPageComponent */],
            __WEBPACK_IMPORTED_MODULE_10__components_login_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_11__components_admin_panel_admin_panel_component__["a" /* AdminPanelComponent */],
            __WEBPACK_IMPORTED_MODULE_14__components_card_card_component__["a" /* CardComponent */],
            __WEBPACK_IMPORTED_MODULE_15__components_navbar_navbar_component__["a" /* NavbarComponent */],
            __WEBPACK_IMPORTED_MODULE_17__components_home_home_component__["a" /* HomeComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */].forRoot(appRoutes),
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["d" /* ReactiveFormsModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
            __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__["ToastModule"].forRoot(),
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["d" /* JsonpModule */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_12__services_data_service__["a" /* DataService */],
            __WEBPACK_IMPORTED_MODULE_16__auth_auth_guard__["a" /* AuthGuard */],
            __WEBPACK_IMPORTED_MODULE_13__services_global_events_manager_service__["a" /* GlobalEventsManagerService */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/auth/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = (function () {
    function AuthGuard(dataService, router) {
        this.dataService = dataService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        if (this.dataService.currentUser == null) {
            this.router.navigate(['/login']);
            return false;
        }
        if (!this.dataService.currentUser.isAdmin && this.dataService.currentUser.isLogged) {
            return true;
        }
        if (this.dataService.currentUser.isAdmin) {
            return true;
        }
        else {
            this.router.navigate(['/login']);
            return false;
        }
    };
    return AuthGuard;
}());
AuthGuard = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_data_service__["a" /* DataService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _b || Object])
], AuthGuard);

var _a, _b;
//# sourceMappingURL=auth.guard.js.map

/***/ }),

/***/ "../../../../../src/app/components/admin-panel/admin-panel.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".text {\r\n    color: #000;\r\n}\r\n\r\n.addQuestion {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: horizontal;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-flow: row wrap;\r\n            flex-flow: row wrap;\r\n    -webkit-box-pack: space-evenly;\r\n        -ms-flex-pack: space-evenly;\r\n            justify-content: space-evenly;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    padding: 20px;\r\n    width: 100%;\r\n}\r\n\r\n\r\n@media screen and (max-width: 991px) and (min-width: 200px) {\r\n\r\n    .btn-smallDevice {\r\n        margin-top: 10px;\r\n    }\r\n\r\n    .btn-smallDevice button {\r\n        width: 100%;\r\n    }\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/admin-panel/admin-panel.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-md-12 d-flex justify-content-start\" style=\"margin-bottom:10px;\">\n      <button type=\"submit\" (click)=\"reset()\" data-toggle=\"modal\" data-target=\"#myModal\" class=\"btn btn-primary\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i> Add Questionnaire</button>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-md-12\">\n      <table class=\"table\">\n        <thead>\n          <tr>\n            <th>Num</th>\n            <th>Name of questionnaire</th>\n            <th></th>\n            <th></th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr *ngFor=\"let q of questionnaires; let i = index \">\n            <th scope=\"row\">{{i + 1}}</th>\n            <td style=\"text-align: left;\">{{q.name}}</td>\n            <td><i class=\"fa fa-pencil-square-o\" data-toggle=\"modal\" data-target=\"#myModal\" aria-hidden=\"true\" style=\"color:thistle\"\n                (click)=\"setCurrentQuestionnaire(q,0)\"></i></td>\n            <td><i class=\"fa fa-times\" data-toggle=\"modal\" data-target=\"#deleteModal\" aria-hidden=\"true\" style=\"color:red\" (click)=\"setCurrentQuestionnaire(q, 1)\">\n            </i></td>\n          </tr>\n        </tbody>\n      </table>\n    </div>\n  </div>\n</div>\n\n<!--Modal-->\n<div class=\"modal fade\" id=\"myModal\" role=\"dialog\" (onHidden)=\"close()\">\n  <div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title text\">New Questionnaire</h4>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"container-fluid\">\n          <div class=\"row\">\n            <div class=\"col-md-12\">\n              <input type=\"text\" class=\"form-control\" [(ngModel)]=\"questionnaire.name\" placeholder=\"Questionnare title\">\n            </div>\n          </div>\n          <div class=\"row d-flex align-items-center\" style=\"margin-top:10px;margin-bottom:10px\" *ngIf=\"!isUpdate\">\n            <div class=\"col-md-3\">\n              <span class=\"text\">Add question: </span>\n            </div>\n            <div class=\"col-md-3\">\n              <select name=\"questionType\" [(ngModel)]=\"question.type\" class=\"form-control\">\n                  <option value=\"1\">Text</option>\n                  <option value=\"2\">YesOrNo</option>\n                  <option value=\"3\">Multiple choice</option>\n                  <option value=\"4\">Single choice</option>\n              </select>\n            </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"col-lg-5\">\n              <div class=\"input-group\" *ngIf=\"!isUpdate\">\n                <input type=\"text\" class=\"form-control\" placeholder=\"Question\" [(ngModel)]=\"question.text\">\n              </div>\n            </div>\n            <div class=\"col-lg-5\" *ngIf=\"question.type > 2 && !isUpdate\">\n              <div class=\"input-group\">\n                <input type=\"text\" class=\"form-control mb-2 mr-sm-2 mb-sm-0\" placeholder=\"Choices\" [(ngModel)]=\"question.choices\" multiple>\n              </div>\n            </div>\n            <div class=\"col-lg-2 btn-smallDevice\" *ngIf=\"!isUpdate\">\n              <div class=\"input-group\">\n                <button class=\"btn btn-primary\" (click)=\"insertQuestion(question)\">Add</button>\n              </div>\n            </div>\n          </div>\n          <div class=\"row align-items-baseline\" style=\"margin:10px 0 10px 0\" *ngFor=\"let que of questions\">\n            <!--questions-->\n            <div class=\"col-md-2\">\n              <span class=\"text\">Num: {{que.questionNum}}</span>\n            </div>\n            <div class=\"col-md-3\">\n              <!--<span class=\"text\">Question: {{que.text}}</span>-->\n              <input type=\"text\" class=\"form-control\" [(ngModel)] = \"que.text\">\n            </div>\n            <div class=\"col-md-3\" [ngSwitch]=\"que.type\">\n              <span class=\"text\" *ngSwitchCase=\"1\">Type:Text</span>\n              <span class=\"text\" *ngSwitchCase=\"2\">Type:YesOrNo</span>\n              <span class=\"text\" *ngSwitchCase=\"3\">Type:Multiple choice</span>\n              <span class=\"text\" *ngSwitchCase=\"4\">Type:Single choice</span>\n            </div>\n            <div class=\"col-md-3\">\n              <span class=\"text\" *ngIf=\"que.type > 2\">Choices: ({{que.choices}})</span>\n            </div>\n            <div class=\"col-md-1\" *ngIf=\"!isUpdate\">\n              <i class=\"fa fa-times\" aria-hidden=\"true\" style=\"color:red\" (click)=\"deleteQuestion(que.questionNum)\"></i>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-12\" *ngIf=\"isError\">\n          <div class=\"form-control-feedback\">\n            <span class=\"text-danger align-middle\">\n                           <i class=\"fa fa-close\"> All fields must be filled out!</i>\n                        </span>\n          </div>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"save()\">Save</button>\n        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" (click)=\"close()\">Close</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<!-- Delete Modal -->\n<div class=\"modal fade\" id=\"deleteModal\" role=\"dialog\">\n  <div class=\"modal-dialog modal-sm\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title text\">Delete Questionnaire</h4>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      </div>\n      <div class=\"modal-body\">\n        <p class=\"text\">Are you sure you want to delete {{questionnaire.name}}?</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-success\" data-dismiss=\"modal\" (click)=\"deleteQuestionnaire()\">Yes</button>\n        <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\">No</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/admin-panel/admin-panel.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminPanelComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_question__ = __webpack_require__("../../../../../src/app/model/question.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_questionnaire__ = __webpack_require__("../../../../../src/app/model/questionnaire.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminPanelComponent = (function () {
    function AdminPanelComponent(dataService, elRef) {
        this.dataService = dataService;
        this.elRef = elRef;
        this.numQuestion = 1;
        this.isUpdate = false;
        this.isError = false;
        this.lastNum = 0;
    }
    AdminPanelComponent.prototype.ngOnInit = function () {
        this.getQuestionnaires();
        this.currUser = this.dataService.currentUser;
        this.question = new __WEBPACK_IMPORTED_MODULE_2__model_question__["a" /* Question */](1, 1, this.numQuestion, 1, "", "", []);
        this.questions = new Array();
        this.questionnaire = new __WEBPACK_IMPORTED_MODULE_3__model_questionnaire__["a" /* Questionnaire */](1, "");
    };
    AdminPanelComponent.prototype.getQuestionnaires = function () {
        var _this = this;
        this.dataService.getQuestionnaires()
            .subscribe(function (data) {
            _this.questionnaires = data;
        }, function (err) {
            console.log(err);
        });
    };
    ;
    AdminPanelComponent.prototype.insertQuestion = function (q) {
        if (this.lastNum != 0) {
            q.questionNum = this.lastNum;
            this.lastNum++;
        }
        ;
        this.questions.push(q);
        this.numQuestion++;
        this.question = new __WEBPACK_IMPORTED_MODULE_2__model_question__["a" /* Question */](1, 1, this.numQuestion, 1, "", "", []);
    };
    AdminPanelComponent.prototype.deleteQuestion = function (row) {
        row--;
        var newRow = 1;
        this.questions.splice(row, 1);
        for (var i = 0; i < this.questions.length; i++) {
            this.questions[i].questionNum = newRow;
            newRow++;
        }
        this.lastNum = newRow;
        (this.questions);
    };
    ;
    AdminPanelComponent.prototype.close = function () {
        this.getQuestionnaires();
        this.isUpdate = false;
        this.numQuestion = 1;
    };
    AdminPanelComponent.prototype.save = function () {
        var _this = this;
        if (this.isUpdate == false) {
            if (this.questions.length == 0 || this.questionnaire.name == "") {
                this.isError = true;
                return;
            }
            ;
            var lastId_1 = 0;
            this.dataService.saveQuestionnaire(this.questionnaire)
                .subscribe(function (data) {
                lastId_1 = data.lastId;
                for (var i = 0; i < _this.questions.length; i++) {
                    if (_this.questions[i].choices.length == 0) {
                        var empty = "";
                        _this.questions[i].choices.push(empty);
                    }
                    _this.questions[i].questionnaireId = lastId_1;
                }
                _this.dataService.saveQuestions(_this.questions)
                    .subscribe(function (data) {
                }, function (err) {
                    console.log(err);
                });
                _this.questions = [];
                _this.getQuestionnaires();
            }, function (err) {
                console.log(err);
            });
        }
        else {
            this.dataService.updateQuestionnaire(this.questionnaire)
                .subscribe(function (data) {
                _this.dataService.updateQuestions(_this.questions)
                    .subscribe(function (data) {
                });
            });
        }
        ;
        this.isUpdate = false;
        this.numQuestion = 1;
        this.isError = false;
        jQuery(this.elRef.nativeElement).find('#myModal').modal('toggle');
    };
    ;
    AdminPanelComponent.prototype.setCurrentQuestionnaire = function (q, type) {
        var _this = this;
        this.questionnaire = q;
        if (type == 0) {
            this.isUpdate = true;
            this.dataService.getQuestions(this.questionnaire.id)
                .subscribe(function (data) {
                _this.questions = data;
            });
        }
    };
    AdminPanelComponent.prototype.deleteQuestionnaire = function () {
        this.dataService.deleteQuestionnaire(this.questionnaire.id)
            .subscribe(function (data) {
        }, function (err) {
        });
        this.getQuestionnaires();
    };
    ;
    AdminPanelComponent.prototype.reset = function () {
        this.questions = new Array();
        this.questionnaire = new __WEBPACK_IMPORTED_MODULE_3__model_questionnaire__["a" /* Questionnaire */](1, "");
        this.isUpdate = false;
        this.numQuestion = 1;
    };
    return AdminPanelComponent;
}());
AdminPanelComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-admin-panel',
        template: __webpack_require__("../../../../../src/app/components/admin-panel/admin-panel.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/admin-panel/admin-panel.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _b || Object])
], AdminPanelComponent);

var _a, _b;
//# sourceMappingURL=admin-panel.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/card/card.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".card {\r\n    border-radius: 0;\r\n    cursor: pointer;\r\n}\r\n.card,\r\n.card-footer {\r\n    background-color: #ffffA5;\r\n    border: none;\r\n}\r\n.card.completed,\r\n.card.completed .card-footer {\r\n    background-color: #EEE;\r\n    color: #2c3531;\r\n}\r\n.card-footer {\r\n    text-align: right;\r\n}\r\ni {\r\n    color: #CC0000;\r\n    padding-right: .5rem;\r\n}\r\n.fa.fa-check {\r\n    color: green;\r\n}\r\n\r\n.card-block p {\r\n    color: #2c3531;\r\n}\r\n\r\n.card-footer span {\r\n    color: #2c3531;\r\n}\r\n\r\nh4 {\r\n    color: #2c3531;\r\n}\r\n\r\n.question {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-flow: column wrap;\r\n            flex-flow: column wrap;\r\n}\r\n\r\nspan {\r\n    color:#2c3531;\r\n}\r\n\r\nlabel {\r\n    color: #2c3531;\r\n    margin: 5px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/card/card.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-sm-12\" style=\"margin-bottom: 15px;\" data-toggle=\"modal\" data-target=\"#questionModal\" (click) = \"getQuestions(questionnaire.id)\">\n    <div class=\"card\">\n        <div class=\"card-block\">\n            <p>{{questionnaire.name}}</p>\n        </div>\n        <div class=\"card-footer\">\n            <span *ngIf=\"questionnaire.done == 0\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i>Not Completed</span>\n            <span *ngIf=\"questionnaire.done == 1\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i>Completed</span>\n        </div>\n    </div>\n</div>\n\n<!--Modal-->\n<div class=\"modal fade\" id=\"questionModal\" role=\"dialog\" (onHidden)=\"close()\">\n    <div class=\"modal-dialog modal-lg\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\">{{questionnaire.name}}</h4>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n            </div>\n            <div class=\"modal-body\">\n\n                <div class=\"d-flex justify-content-center\" *ngFor=\"let q of question; let i = index\" style=\"margin-bottom: 10px;\">\n                    <div class=\"col-md-3\">\n                        <span>{{q.text}}</span>\n                    </div>\n                    <div class=\"col-md-9\" [ngSwitch]=\"q.type\">\n                        <div *ngSwitchCase=\"1\">\n                            <input type=\"text\" class=\"form-control\" name=\"Answer {{i}}\" [(ngModel)]=\"questionInsert[i].choice\" placeholder=\"Answer...\">\n                        </div>\n                        <div *ngSwitchCase=\"2\">\n                            <label><input type=\"radio\" name=\"yesNo\" value=\"Yes\" checked [(ngModel)]=\"questionInsert[i].choice\"> Yes</label>\n                            <label><input type=\"radio\" name=\"yesNo\" value=\"No\" [(ngModel)]=\"questionInsert[i].choice\"> No</label>\n                        </div>\n                        <div *ngSwitchCase=\"3\">\n                            <label class=\"checkbox-inline\" *ngFor=\"let choice of q.choices; let x = index;\"><input type=\"checkbox\" name=\"multipleAnswer {{i}}, {{x}}\" [(ngModel)]=\"questionChoice[x]\" value=\"{{choice}}\"> {{choice}}</label>\n                        </div>\n                        <div *ngSwitchCase=\"4\">\n                            <label *ngFor=\"let choice of q.choices; let y = index;\"><input type=\"radio\" name=\"optradio\" [(ngModel)]=\"questionInsert[i].choices\" value=\"{{choice}}\"> {{choice}}</label>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-md-12\" *ngIf=\"isError\">\n                    <div class=\"form-control-feedback\">\n                        <span class=\"text-danger align-middle\">\n                           <i class=\"fa fa-close\"> All fields must be filled out!</i>\n                        </span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"submit\" class=\"btn btn-primary\" (click)=\"save()\">Save</button>\n                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/card/card.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model_questionnaire__ = __webpack_require__("../../../../../src/app/model/questionnaire.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__ = __webpack_require__("../../../../ng2-toastr/ng2-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CardComponent = (function () {
    function CardComponent(dataService, toasts, vcr, renderer) {
        this.dataService = dataService;
        this.toasts = toasts;
        this.renderer = renderer;
        this.questionChoice = [];
        this.isError = false;
        this.toasts.setRootViewContainerRef(vcr);
    }
    CardComponent.prototype.ngOnInit = function () {
        //this.getQuestions();
        console.log(this.dataService.currentUser);
        this.questionChoice = new Array();
    };
    CardComponent.prototype.ngAfterViewInit = function () {
    };
    CardComponent.prototype.statusToggle = function () {
    };
    CardComponent.prototype.deleteTask = function () {
    };
    CardComponent.prototype.getQuestions = function (id) {
        var _this = this;
        this.question = [];
        this.questionInsert = [];
        console.log(this.question);
        console.log(this.questionInsert);
        console.log(id);
        this.dataService.getQuestions(id)
            .subscribe(function (data) {
            console.log(data);
            _this.question = data;
            _this.question.forEach(function (el) {
                if (el.choices.length > 0) {
                    var ch = el.choices.toString();
                    el.choices = ch.split(",");
                }
                ;
            });
            _this.questionInsert = data;
        }, function (err) {
            console.log(err);
        });
        console.log(this.question);
        console.log(this.questionInsert);
    };
    ;
    CardComponent.prototype.save = function () {
        var _this = this;
        console.log(this.questionInsert);
        console.log(this.questionChoice);
        var startNum = 0;
        var answers = new Array();
        this.isError = false;
        this.questionInsert.forEach(function (el) {
            var _el = Object.assign({}, el);
            if (_el["type"] == 3) {
            }
            var answer = new Array(_el["questionnareId"], 1, _el["questionNum"], el.choice);
            answers.push(answer);
        });
        for (var i = 0; i < answers.length; i++) {
            if (answers[i][3] == "" || answers[i][3] == null) {
                this.isError = true;
                return;
            }
            ;
        }
        ;
        this.dataService.saveUserAnswers(answers)
            .subscribe(function (data) {
            console.log(data);
            if (data.type == 0) {
                _this.toasts.success("You have successfully completed this questionnaire!", 'Success');
            }
        }, function (err) {
            console.log(err);
        });
    };
    ;
    CardComponent.prototype.close = function () {
        this.question = [];
        this.questionInsert = [];
    };
    return CardComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('closeModal'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _a || Object)
], CardComponent.prototype, "closeModal", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__model_questionnaire__["a" /* Questionnaire */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__model_questionnaire__["a" /* Questionnaire */]) === "function" && _b || Object)
], CardComponent.prototype, "questionnaire", void 0);
CardComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-card',
        template: __webpack_require__("../../../../../src/app/components/card/card.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/card/card.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__["ToastsManager"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__["ToastsManager"]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]) === "function" && _f || Object])
], CardComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=card.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".questionnaires {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: horizontal;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-flow: row wrap;\r\n            flex-flow: row wrap;\r\n    -ms-flex-pack: distribute;\r\n        justify-content: space-around;\r\n    width: 100%;\r\n}\r\n\r\n.completed {\r\n    background-color: #95a5a6 !important;\r\n    cursor: not-allowed !important;\r\n    color: #ffffA5 !important;\r\n    pointer-events: none !important;\r\n}\r\n.notClickable {\r\n    pointer-events: none !important;\r\n}\r\n\r\n.card {\r\n    border-radius: 0;\r\n    cursor: pointer;\r\n}\r\n.card,\r\n.card-footer {\r\n    background-color: #ffffA5;\r\n    border: none;\r\n}\r\n.card.completed,\r\n.card.completed .card-footer {\r\n    background-color: #EEE;\r\n    color: #2c3531;\r\n}\r\n.card-footer {\r\n    text-align: right;\r\n}\r\ni {\r\n    color: #CC0000;\r\n    padding-right: .5rem;\r\n}\r\n.fa.fa-check {\r\n    color: green;\r\n}\r\n\r\n.card-block p {\r\n    color: #2c3531;\r\n}\r\n\r\n.card-footer span {\r\n    color: #2c3531;\r\n}\r\n\r\nh4 {\r\n    color: #2c3531;\r\n}\r\n\r\n.question {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-flow: column wrap;\r\n            flex-flow: column wrap;\r\n}\r\n\r\nspan {\r\n    color:#2c3531;\r\n}\r\n\r\nlabel {\r\n    color: #2c3531;\r\n    margin: 5px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<!--<div class=\"row\">\n  <div class=\"questionnaires\">\n    <app-card *ngFor=\"let q of questionnaire\" [questionnaire]=\"q\"></app-card>\n    <h5 *ngIf=\"questionnaire.length == 0\">\n      <p>The questionnaire list is empty</p>\n      <p>Wait for new to come</p>\n    </h5>\n  </div>\n</div> -->\n\n<div class=\"row\">\n  <div class=\"questionnaires\">\n    <div class=\"col-sm-4\" *ngFor=\"let q of questionnaire\" style=\"margin-bottom: 15px;\" data-toggle=\"modal\" data-target=\"#questionModal\" \n      (click)=\"getQuestions(q.id)\" [ngClass] = \"{'notClickable': q.done == 1}\">\n      <div class=\"card\" [ngClass] = \"{'completed': q.done == 1}\">\n        <div class=\"card-block\">\n          <p>{{q.name}}</p>\n        </div>\n        <div class=\"card-footer\" [ngClass] = \"{'completed': q.done == 1}\">\n          <span *ngIf=\"q.done == 0\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i>Not Completed</span>\n          <span *ngIf=\"q.done == 1\"><i class=\"fa fa-check\" aria-hidden=\"true\"></i>Completed</span>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<!--Modal-->\n<div class=\"modal fade\" id=\"questionModal\" role=\"dialog\" (onHidden)=\"close()\">\n  <div class=\"modal-dialog modal-lg\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\">{{questionnaire.name}}</h4>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      </div>\n      <div class=\"modal-body\">\n\n        <div class=\"d-flex justify-content-center\" *ngFor=\"let q of question; let i = index\" style=\"margin-bottom: 10px;\">\n          <div class=\"col-md-3\">\n            <span>{{q.text}}</span>\n          </div>\n          <div class=\"col-md-9\" [ngSwitch]=\"q.type\">\n            <div *ngSwitchCase=\"1\">\n              <input type=\"text\" class=\"form-control\" name=\"Answer {{i}}\" [(ngModel)]=\"questionInsert[i].choice\" placeholder=\"Answer...\">\n            </div>\n            <div *ngSwitchCase=\"2\">\n              <label><input type=\"radio\" name=\"yesNo + {{i}}\" value=\"Yes\" checked [(ngModel)]=\"questionInsert[i].choice\"> Yes</label>\n              <label><input type=\"radio\" name=\"yesNo + {{i}}\" value=\"No\" [(ngModel)]=\"questionInsert[i].choice\"> No</label>\n            </div>\n            <div *ngSwitchCase=\"3\">\n              <label class=\"checkbox-inline\" *ngFor=\"let choice of q.choices; let x = index;\"><input type=\"checkbox\" name=\"multipleAnswer {{i}}, {{x}}\" [(ngModel)]=\"questionChoice[x]\" value=\"{{choice}}\"> {{choice}}</label>\n            </div>\n            <div *ngSwitchCase=\"4\">\n              <label *ngFor=\"let choice of q.choices; let y = index;\"><input type=\"radio\" name=\"optradio\" [(ngModel)]=\"questionInsert[i].choice\" value=\"{{choice}}\"> {{choice}}</label>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-12\" *ngIf=\"isError\">\n          <div class=\"form-control-feedback\">\n            <span class=\"text-danger align-middle\">\n                           <i class=\"fa fa-close\"> All fields must be filled out!</i>\n                        </span>\n          </div>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"submit\" class=\"btn btn-primary\" (click)=\"save()\">Save</button>\n        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_toastr_ng2_toastr__ = __webpack_require__("../../../../ng2-toastr/ng2-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng2_toastr_ng2_toastr__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeComponent = (function () {
    function HomeComponent(dataService, toasts, vcr, elRef) {
        this.dataService = dataService;
        this.toasts = toasts;
        this.elRef = elRef;
        this.questionChoice = [];
        this.isError = false;
        this.toasts.setRootViewContainerRef(vcr);
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.questionnaire = new Array();
        this.getQuestionnairesForUsers();
    };
    ;
    HomeComponent.prototype.getQuestionnairesForUsers = function () {
        var _this = this;
        var userId = this.dataService.currentUser.id;
        this.dataService.getQuestionnairesForUsers(userId)
            .subscribe(function (data) {
            _this.questionnaire = data;
        });
        console.log(this.questionnaire);
    };
    HomeComponent.prototype.getQuestions = function (id) {
        var _this = this;
        this.dataService.getQuestions(id)
            .subscribe(function (data) {
            _this.question = data;
            _this.question.forEach(function (el) {
                if (el.choices.length > 0) {
                    var ch = el.choices.toString();
                    el.choices = ch.split(",");
                }
                ;
            });
            _this.questionInsert = data;
        }, function (err) {
            console.log(err);
        });
    };
    ;
    HomeComponent.prototype.save = function () {
        var _this = this;
        var userId = this.dataService.currentUser.id;
        var startNum = 0;
        var answers = new Array();
        this.isError = false;
        this.questionInsert.forEach(function (el) {
            var _el = Object.assign({}, el);
            if (_el["type"] == 3) {
                el.choice = _this.questionChoice;
            }
            var answer = new Array(_el["questionnareId"], userId, _el["questionNum"], el.choice.toString());
            answers.push(answer);
        });
        for (var i = 0; i < answers.length; i++) {
            if (answers[i][3] == "" || answers[i][3] == null) {
                this.isError = true;
                return;
            }
            ;
        }
        ;
        this.dataService.saveUserAnswers(answers)
            .subscribe(function (data) {
            if (data.type == 0) {
                _this.toasts.success("You have successfully completed this questionnaire!", 'Success');
            }
        }, function (err) {
            console.log(err);
        });
        jQuery(this.elRef.nativeElement).find('#questionModal').modal('toggle');
        this.getQuestionnairesForUsers();
    };
    ;
    HomeComponent.prototype.close = function () {
        this.getQuestionnairesForUsers();
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-home',
        template: __webpack_require__("../../../../../src/app/components/home/home.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/home/home.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_data_service__["a" /* DataService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_toastr_ng2_toastr__["ToastsManager"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_toastr_ng2_toastr__["ToastsManager"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]) === "function" && _d || Object])
], HomeComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=home.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/landing-page/landing-page.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".lead {\r\n    padding-top: 5px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/landing-page/landing-page.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"inner cover\">\n  <div class=\"cover-heading\"><img src=\"./assets/questionnaire-design.png\" /></div>\n  <p class=\"lead\">Welcome to the official MOP Questionnaire</p>\n  <p class=\"lead\">If you don't have an account register or just sign in!</p>\n  <p class=\"lead\">\n    <a routerLink=\"/register\" class=\"btn btn-lg btn-secondary register\" (click)=\"addNavbar()\">Register</a>\n    <a routerLink=\"/login\" class=\"btn btn-lg btn-secondary sign-in\" (click)=\"addNavbar()\">Sign in</a>\n  </p>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/landing-page/landing-page.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LandingPageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_global_events_manager_service__ = __webpack_require__("../../../../../src/app/services/global-events-manager.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LandingPageComponent = (function () {
    function LandingPageComponent(gEM) {
        this.gEM = gEM;
    }
    LandingPageComponent.prototype.ngOnInit = function () {
    };
    LandingPageComponent.prototype.addNavbar = function () {
        this.gEM.showBackNavBar(true);
    };
    return LandingPageComponent;
}());
LandingPageComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-landing-page',
        template: __webpack_require__("../../../../../src/app/components/landing-page/landing-page.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/landing-page/landing-page.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_global_events_manager_service__["a" /* GlobalEventsManagerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_global_events_manager_service__["a" /* GlobalEventsManagerService */]) === "function" && _a || Object])
], LandingPageComponent);

var _a;
//# sourceMappingURL=landing-page.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media screen and (max-width: 991px) and (min-width: 520px) {\r\n\r\n    .smallDevice {\r\n        margin-bottom: 15px;\r\n    }\r\n\r\n    .smallDevice button {\r\n        width: 100% !important;\r\n    }\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <form class=\"form-horizontal\"\n    [formGroup]=\"loginForm\"\n    (ngSubmit) = \"login(loginForm.value)\"\n     role=\"form\">\n     <div class=\"row\">\n            <div class=\"col-md-3\"></div>\n            <div class=\"col-md-6\">\n                <h2>Login</h2>\n                <hr>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-3 field-label-responsive\">\n                <label for=\"email\">E-Mail Address</label>\n            </div>\n            <div class=\"col-md-6\">\n                <div class=\"form-group\">\n                    <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">\n                        <div class=\"input-group-addon\" style=\"width: 2.6rem\"><i class=\"fa fa-at\"></i></div>\n                        <input \n                        type=\"text\" \n                        name=\"email\" class=\"form-control\" \n                        id=\"email\" \n                        placeholder=\"you@example.com\"\n                        formControlName=\"email\" \n                        email\n                        required autofocus>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-3\">\n                <div class=\"form-control-feedback\">\n                    <span class=\"text-danger align-middle\"\n                     *ngIf=\"!loginForm.controls.email.valid && loginForm.controls.email.touched\">\n                           <i class=\"fa fa-close\"> Email field is required!</i>\n                        </span>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-md-3 field-label-responsive\">\n                <label for=\"password\">Password</label>\n            </div>\n            <div class=\"col-md-6\">\n                <div class=\"form-group\">\n                    <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">\n                        <div class=\"input-group-addon\" style=\"width: 2.6rem\"><i class=\"fa fa-key\"></i></div>\n                        <input\n                         type=\"password\" \n                         name=\"password\" \n                         class=\"form-control\" \n                         id=\"password\" \n                         placeholder=\"Password\" \n                         formControlName=\"password\"\n                         required>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-md-3\">\n                <div class=\"form-control-feedback\">\n                   <span class=\"text-danger align-middle\"\n                     *ngIf=\"!loginForm.controls.password.valid && loginForm.controls.password.touched\">\n                           <i class=\"fa fa-close\"> Password field is required!</i>\n                        </span>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\" style=\"margin-bottom: 10px;\">\n            <div class=\"col-md-12\">\n                <span class=\"text-danger align-middle\"\n                     *ngIf=\"isError\">\n                           <i class=\"fa fa-close\"> {{errorMessage}}</i>\n                </span>\n            </div>\n        </div>\n        <div class=\"row justify-content-md-center\">\n            <div class=\"col-md-3 smallDevice\">\n                <button \n                type=\"submit\" \n                class=\"btn btn-success\"\n                [disabled]=\"!loginForm.valid\"\n                ><i class=\"fa fa-user-plus\"></i> Sign in</button>\n            </div>\n            <div class=\"col-md-3 smallDevice\">\n              <button \n                (click) = \"cancel()\"\n                class=\"btn btn-danger\"\n                ><i class=\"fa fa-eraser\"></i> Cancel</button>\n            </div>\n        </div>\n    </form>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_global_events_manager_service__ = __webpack_require__("../../../../../src/app/services/global-events-manager.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = (function () {
    function LoginComponent(router, dataService, gEM) {
        this.router = router;
        this.dataService = dataService;
        this.gEM = gEM;
        this.errorMessage = "";
        this.isError = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.configForm();
    };
    LoginComponent.prototype.configForm = function () {
        this.loginForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */]({
            email: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
            password: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].minLength(6))
        });
    };
    LoginComponent.prototype.cancel = function () {
        this.configForm();
    };
    LoginComponent.prototype.login = function (model) {
        var _this = this;
        this.isError = false;
        this.dataService.loginCheck(model.email, model.password)
            .subscribe(function (data) {
            if (data.type == 1) {
                _this.errorMessage = data.msg;
                _this.isError = true;
                return;
            }
            model.isAdmin = data.is_admin;
            model.lastName = data.lastName;
            model.name = data.name;
            model.id = data.id;
            model.isLogged = true;
            if (model.isAdmin) {
                _this.setCurrentUser(model);
                _this.router.navigate(['/admin-panel']);
                _this.gEM.showNavBar(true);
                _this.gEM.showBackNavBar(false);
                _this.gEM.showName(model.name);
            }
            else {
                _this.setCurrentUser(model);
                console.log(model);
                _this.gEM.showNavBar(true);
                _this.gEM.showBackNavBar(false);
                _this.gEM.showName(model.name);
                _this.router.navigate(['/home']);
            }
        }, function (err) {
            console.log(err);
        });
    };
    LoginComponent.prototype.setCurrentUser = function (curUser) {
        this.dataService.currentUser = curUser;
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-login',
        template: __webpack_require__("../../../../../src/app/components/login/login.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/login/login.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_data_service__["a" /* DataService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__services_global_events_manager_service__["a" /* GlobalEventsManagerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_global_events_manager_service__["a" /* GlobalEventsManagerService */]) === "function" && _c || Object])
], LoginComponent);

var _a, _b, _c;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".navBtn {\r\n    padding: 10px;\r\n    text-decoration: none;\r\n    border: 1px solid #d9b08c;\r\n    margin-left: 10px;\r\n    border-radius: 5px;\r\n}\r\n\r\n.navBtn:hover {\r\n    background-color: #d1e8e2;\r\n    color: #116466;\r\n    border: 1px solid #116466;\r\n}\r\n\r\n@media screen and (max-width: 750px) and (min-width: 200px) {\r\n    .navSmall {\r\n        margin: 20px 0 20px 0;\r\n    }\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"showBackNavBar\" class=\"navSmall\">\n    <a class=\"navBtn\" routerLink=\"/\" (click) = \"back()\" ><i class=\"fa fa-backward\" aria-hidden=\"true\"></i> Back</a>\n</div>\n<div *ngIf=\"showNavBar\" class=\"navSmall\">\n    <span>Welcome {{user}}</span>\n    <a class=\"navBtn\" routerLink=\"/\" (click) = \"logout()\"><i class=\"fa fa-backward\" aria-hidden=\"true\"></i> Logout</a>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_global_events_manager_service__ = __webpack_require__("../../../../../src/app/services/global-events-manager.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__model_user__ = __webpack_require__("../../../../../src/app/model/user.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NavbarComponent = (function () {
    function NavbarComponent(gEM, DataService) {
        var _this = this;
        this.gEM = gEM;
        this.DataService = DataService;
        this.showNavBar = false;
        this.showBackNavBar = false;
        this.gEM.showNavBarEmitter.subscribe(function (mode) {
            // mode will be null the first time it is created, so you need to igonore it when null
            if (mode !== null) {
                _this.showNavBar = mode;
            }
        });
        this.gEM.showBackNavBarEmitter.subscribe(function (mode) {
            if (mode !== null) {
                _this.showBackNavBar = mode;
            }
        });
        this.gEM.showNameEmmiter.subscribe(function (mode) {
            if (mode !== null) {
                _this.user = mode;
            }
        });
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.back = function () {
        this.gEM.showBackNavBar(false);
    };
    NavbarComponent.prototype.logout = function () {
        var resetUser = new __WEBPACK_IMPORTED_MODULE_3__model_user__["a" /* User */](1, "", "", "", "", "", false, false);
        this.gEM.showNavBar(false);
        this.DataService.currentUser = resetUser;
    };
    return NavbarComponent;
}());
NavbarComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-navbar',
        template: __webpack_require__("../../../../../src/app/components/navbar/navbar.component.html"),
        styles: [__webpack_require__("../../../../../src/app/components/navbar/navbar.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_global_events_manager_service__["a" /* GlobalEventsManagerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_global_events_manager_service__["a" /* GlobalEventsManagerService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_data_service__["a" /* DataService */]) === "function" && _b || Object])
], NavbarComponent);

var _a, _b;
//# sourceMappingURL=navbar.component.js.map

/***/ }),

/***/ "../../../../../src/app/model/question.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Question; });
var Question = (function () {
    function Question(id, questionnaireId, questionNum, type, text, choice, choices) {
        this.id = id;
        this.questionnaireId = questionnaireId;
        this.questionNum = questionNum;
        this.type = type;
        this.text = text;
        this.choice = choice;
        this.choices = choices;
    }
    return Question;
}());

//# sourceMappingURL=question.js.map

/***/ }),

/***/ "../../../../../src/app/model/questionnaire.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Questionnaire; });
var Questionnaire = (function () {
    function Questionnaire(id, name) {
        this.id = id;
        this.name = name;
    }
    return Questionnaire;
}());

//# sourceMappingURL=questionnaire.js.map

/***/ }),

/***/ "../../../../../src/app/model/user.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = (function () {
    function User(id, name, lastName, email, password, confirmPassword, isAdmin, isLogged) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.isAdmin = isAdmin;
        this.isLogged = isLogged;
    }
    return User;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ "../../../../../src/app/register/register.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <form class=\"form-horizontal\"\r\n     role=\"form\"\r\n     (ngSubmit)=\"register(registerForm.value)\"\r\n     [formGroup] = \"registerForm\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-3\"></div>\r\n            <div class=\"col-md-6\">\r\n                <h2>Register New User</h2>\r\n                <hr>\r\n            </div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col-md-3 field-label-responsive\">\r\n                <label for=\"name\">Name</label>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n                <div class=\"form-group\">\r\n                    <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">\r\n                        <div class=\"input-group-addon\" style=\"width: 2.6rem\"><i class=\"fa fa-user\"></i></div>\r\n                        <input\r\n                         type=\"text\" \r\n                         name=\"name\" \r\n                         class=\"form-control\" \r\n                         id=\"name\" \r\n                         placeholder=\"John\"\r\n                         formControlName=\"name\"\r\n                         required autofocus>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-3\">\r\n                <div class=\"form-control-feedback\">\r\n                    <span class=\"text-danger align-middle\">\r\n                            <span class=\"text-danger align-middle\"\r\n                     *ngIf=\"!registerForm.controls.name.valid && registerForm.controls.name.touched\">\r\n                           <i class=\"fa fa-close\"> Name field is required!</i>\r\n                        </span>\r\n                        </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col-md-3 field-label-responsive\">\r\n                <label for=\"name\">Last name</label>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n                <div class=\"form-group\">\r\n                    <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">\r\n                        <div class=\"input-group-addon\" style=\"width: 2.6rem\"><i class=\"fa fa-user\"></i></div>\r\n                        <input\r\n                         type=\"text\" \r\n                         name=\"last_name\" \r\n                         class=\"form-control\" \r\n                         id=\"last_name\" \r\n                         placeholder=\"Doe\"\r\n                         formControlName=\"last_name\"\r\n                         required autofocus>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-3\">\r\n                <div class=\"form-control-feedback\">\r\n                    <span class=\"text-danger align-middle\">\r\n                            <span class=\"text-danger align-middle\"\r\n                     *ngIf=\"!registerForm.controls.last_name.valid && registerForm.controls.last_name.touched\">\r\n                           <i class=\"fa fa-close\"> Last name field is required!</i>\r\n                        </span>\r\n                        </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col-md-3 field-label-responsive\">\r\n                <label for=\"email\">E-Mail Address</label>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n                <div class=\"form-group\">\r\n                    <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">\r\n                        <div class=\"input-group-addon\" style=\"width: 2.6rem\"><i class=\"fa fa-at\"></i></div>\r\n                        <input \r\n                        type=\"text\" \r\n                        name=\"email\" class=\"form-control\" \r\n                        id=\"email\" \r\n                        placeholder=\"you@example.com\"\r\n                        formControlName=\"email\" \r\n                        email\r\n                        required autofocus>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-3\">\r\n                <div class=\"form-control-feedback\">\r\n                    <span class=\"text-danger align-middle\"\r\n                     *ngIf=\"!registerForm.controls.email.valid && registerForm.controls.email.touched\">\r\n                           <i class=\"fa fa-close\"> Email field is required!</i>\r\n                        </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col-md-3 field-label-responsive\">\r\n                <label for=\"password\">Password</label>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n                <div class=\"form-group\">\r\n                    <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">\r\n                        <div class=\"input-group-addon\" style=\"width: 2.6rem\"><i class=\"fa fa-key\"></i></div>\r\n                        <input\r\n                         type=\"password\" \r\n                         name=\"password\" \r\n                         class=\"form-control\" \r\n                         id=\"password\" \r\n                         placeholder=\"Password\" \r\n                         formControlName=\"password\"\r\n                         required>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-3\">\r\n                <div class=\"form-control-feedback\">\r\n                   <span class=\"text-danger align-middle\"\r\n                     *ngIf=\"!registerForm.controls.password.valid && registerForm.controls.password.touched\">\r\n                           <i class=\"fa fa-close\"> Password field is required and must have at least 6 characters!</i>\r\n                        </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col-md-3 field-label-responsive\">\r\n                <label for=\"password\">Confirm Password</label>\r\n            </div>\r\n            <div class=\"col-md-6\">\r\n                <div class=\"form-group\">\r\n                    <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">\r\n                        <div class=\"input-group-addon\" style=\"width: 2.6rem\">\r\n                            <i class=\"fa fa-repeat\"></i>\r\n                        </div>\r\n                        <input \r\n                        type=\"password\" \r\n                        name=\"confirmPassword\" \r\n                        class=\"form-control\" \r\n                        id=\"confirmPassword\" \r\n                        formControlName=\"confirmPassword\"\r\n                        placeholder=\"Password\" \r\n                        required>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n             <div class=\"col-md-3\">\r\n                <div class=\"form-control-feedback\">\r\n                   <span class=\"text-danger align-middle\"\r\n                     *ngIf=\"!registerForm.controls.confirmPassword.valid && registerForm.controls.confirmPassword.touched\">\r\n                           <i class=\"fa fa-close\"> Confirm password field is required!</i>\r\n                        </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n         <div class=\"row\" style=\"margin-bottom: 10px;\">\r\n            <div class=\"col-md-12\">\r\n                <span class=\"text-danger align-middle\"\r\n                     *ngIf=\"isError\">\r\n                           <i class=\"fa fa-close\"> {{errorMessage}}</i>\r\n                </span>\r\n            </div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col-md-3\"></div>\r\n            <div class=\"col-md-6\">\r\n                <button \r\n                type=\"submit\" \r\n                class=\"btn btn-success\"\r\n                [disabled]=\"!registerForm.valid\"\r\n                ><i class=\"fa fa-user-plus\"></i> Register</button>\r\n            </div>\r\n        </div>\r\n    </form>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_data_service__ = __webpack_require__("../../../../../src/app/services/data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__ = __webpack_require__("../../../../ng2-toastr/ng2-toastr.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RegisterComponent = (function () {
    function RegisterComponent(dataService, toasts, vcr, router) {
        this.dataService = dataService;
        this.toasts = toasts;
        this.router = router;
        this.isError = false;
        this.errorMessage = "";
        this.toasts.setRootViewContainerRef(vcr);
    }
    RegisterComponent.prototype.ngOnInit = function () {
        this.configureForm();
    };
    RegisterComponent.prototype.configureForm = function () {
        this.registerForm = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */]({
            name: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]("", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required]),
            last_name: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]("", __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
            email: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]("", __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required),
            password: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */]("", __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].minLength(6)),
            confirmPassword: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */](this.confirmPassword, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].minLength(6))
        });
    };
    RegisterComponent.prototype.register = function (model) {
        var _this = this;
        this.isError = false;
        if (model.password != model.confirmPassword) {
            this.isError = true;
            this.errorMessage = "Passwords don't match!";
            return;
        }
        this.dataService.registerUser(model)
            .subscribe(function (data) {
            if (data.type == 1) {
                _this.isError = true;
                _this.errorMessage = data.msg;
                return;
            }
            _this.toasts.success("You are successfully registered!", 'Success');
            setTimeout(function () {
                _this.router.navigate(['/login']);
            }, 2000);
        }, function (err) {
            console.log(err);
        });
    };
    return RegisterComponent;
}());
RegisterComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-register',
        template: __webpack_require__("../../../../../src/app/register/register.component.html"),
        styles: [__webpack_require__("../../../../../src/app/register/register.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_data_service__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_data_service__["a" /* DataService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]) === "function" && _d || Object])
], RegisterComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=register.component.js.map

/***/ }),

/***/ "../../../../../src/app/services/data.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DataService = (function () {
    function DataService(http, _jsonp) {
        this.http = http;
        this._jsonp = _jsonp;
    }
    DataService.prototype.isCurrentUserLogged = function () {
        return this.currentUser.isLogged;
    };
    DataService.prototype.loginCheck = function (email, password) {
        return this.http.post('api/user', { email: email, password: password })
            .map(function (res) {
            return res.json();
        })
            .catch(function (err) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(err);
        });
    };
    DataService.prototype.registerUser = function (user) {
        return this.http.post('api/register', {
            user: user
        })
            .map(function (res) {
            return res.json();
        })
            .catch(function (err) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(err);
        });
    };
    DataService.prototype.saveQuestionnaire = function (q) {
        return this.http.post('/api/questionnaire', { data: q })
            .map(function (res) {
            return res.json();
        })
            .catch(function (err) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(err);
        });
    };
    ;
    DataService.prototype.saveQuestions = function (q) {
        console.log(q);
        return this.http.post('/api/questions', q)
            .map(function (res) {
            return res.json();
        })
            .catch(function (err) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(err);
        });
    };
    ;
    DataService.prototype.getQuestionnaires = function () {
        return this.http.post('/api/questionnaires', [])
            .map(function (res) {
            return res.json();
        })
            .catch(function (err) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(err);
        });
    };
    ;
    DataService.prototype.getQuestionnairesForUsers = function (id) {
        return this.http.post('/api/questionnairesUser', { data: id })
            .map(function (res) {
            return res.json();
        })
            .catch(function (err) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(err);
        });
    };
    ;
    DataService.prototype.deleteQuestionnaire = function (id) {
        return this.http.post('api/deleteQuestionnaire', { data: id })
            .map(function (res) {
            return res.json();
        })
            .catch(function (err) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(err);
        });
    };
    DataService.prototype.getQuestions = function (id) {
        return this.http.post('api/questionsFrom', { data: id })
            .map(function (res) {
            return res.json();
        })
            .catch(function (err) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(err);
        });
    };
    ;
    DataService.prototype.saveUserAnswers = function (q) {
        return this.http.post('/api/useranswers', q)
            .map(function (res) {
            return res.json();
        })
            .catch(function (err) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(err);
        });
    };
    ;
    DataService.prototype.updateQuestionnaire = function (q) {
        return this.http.post('api/updateQuestionnaire', q)
            .map(function (res) {
            return res.json();
        })
            .catch(function (err) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(err);
        });
    };
    ;
    DataService.prototype.updateQuestions = function (q) {
        return this.http.post('api/updateQuestions', q)
            .map(function (res) {
            return res.json();
        })
            .catch(function (err) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(err);
        });
    };
    ;
    return DataService;
}());
DataService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Jsonp */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Jsonp */]) === "function" && _b || Object])
], DataService);

var _a, _b;
//# sourceMappingURL=data.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/global-events-manager.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalEventsManagerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GlobalEventsManagerService = (function () {
    function GlobalEventsManagerService() {
        this._showNavBar = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.showNavBarEmitter = this._showNavBar.asObservable();
        this._showBackNavBar = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.showBackNavBarEmitter = this._showBackNavBar.asObservable();
        this._showName = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.showNameEmmiter = this._showName.asObservable();
    }
    GlobalEventsManagerService.prototype.showNavBar = function (ifShow) {
        this._showNavBar.next(ifShow);
    };
    GlobalEventsManagerService.prototype.showBackNavBar = function (ifShow) {
        this._showBackNavBar.next(ifShow);
    };
    GlobalEventsManagerService.prototype.showName = function (name) {
        this._showName.next(name);
    };
    return GlobalEventsManagerService;
}());
GlobalEventsManagerService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], GlobalEventsManagerService);

//# sourceMappingURL=global-events-manager.service.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map