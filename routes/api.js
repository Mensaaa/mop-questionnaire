const express = require('express');
const router = express.Router();
const mysql = require('mysql');

mysql://b48c16f446e46b:ebe48474@us-cdbr-iron-east-05.cleardb.net/heroku_a72b2e87a17d673?reconnect=true

/**const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'mensur',
    database: 'questionnaire',
    multipleStatements: true,
});**/

/*var connection = mysql.createConnection({
    host: 'us-cdbr-iron-east-05.cleardb.net',
    user: 'b48c16f446e46b',
    password: 'ebe48474',
    database: 'heroku_a72b2e87a17d673',
    multipleStatements: true,
}
);*/

var connection = mysql.createPool({
    connectionLimit: 10,
    host: 'us-cdbr-iron-east-05.cleardb.net',
    user: 'b48c16f446e46b',
    password: 'ebe48474',
    database: 'heroku_a72b2e87a17d673',
    multipleStatements: true,
}
);

/* GET api listing. */
router.post('/user', (req, res) => {
    let query = "Select * from Users where email = ? and password = ?";
    let params = [req.body.email, req.body.password];

    executeQuery(query, params, function (err, result) {
        if (err)
            console.log(err);
        else {
            if (result.length > 0) {
                res.send(result[0]);
            } else {
                let message = {
                    type: 1,
                    msg: "Email or Password is invalid!"
                }
                res.send(message);
            }

        }

    });
});

router.post('/register', (req, res) => {
    let query = "INSERT into Users(name,last_name,email,password,is_admin,date_created) values(?,?,?,?,0,NOW())";
    let doesExistQuery = "SELECT * from Users where email = ?";
    var message;
    let values = [
        req.body.user.name,
        req.body.user.last_name,
        req.body.user.email,
        req.body.user.password
    ];

    executeQuery(doesExistQuery, [req.body.user.email], (err, result) => {
        if (err)
            console.log(err);
        else {
            if (result.length > 0) {
                message = {
                    type: 1,
                    msg: "Email already exists! Choose another one."
                };
                res.send(message);
            } else {
                executeQuery(query, values, (err, result) => {
                    if (err)
                        console.log(err)
                    else {
                        message = {
                            type: 0,
                            msg: "Success"
                        }
                        res.send(message);
                    }
                });
            }
        }
    });
});

router.post('/questionnaire', (req, res) => {
    let query = "INSERT into questionnaire(name,date_created) values(?,NOW())"
    let values = [req.body.data.name]

    executeQuery(query, values, (err, result) => {
        if (err)
            console.log(err);
        else {
            let message = {
                type: 0,
                text: "success",
                lastId: result.insertId
            }
            console.log(result.insertId);
            res.send(message);
        }
    })
});

router.post('/questions', (req, res) => {
    let query = "INSERT INTO questions(questionnareId,questionNum,type,text,choices) values ?;";
    var values = req.body;
    console.log("alooo: " + req.body)
    var finalArray = [];
    for (var i = 0; i < values.length; i++) {
        console.log(values[i]);
        var tempArray = [];
        tempArray.push(values[i].questionnaireId, values[i].questionNum, values[i].type, values[i].text, values[i].choices);
        finalArray.push(tempArray);
    }
    console.log(finalArray);
    executeQuery(query, [finalArray], (err, result) => {
        if (err)
            console.log(err);
        else {
            let message = {
                type: 0,
                text: "success"
            }
            res.send(message);
        }
    });

});


router.post('/questionnaires', (req, res) => {
    let query = "Select id, name from questionnaire";
    let values = [];

    executeQuery(query, values, (err, result) => {
        if (err)
            console.log(err);
        else {
            let message = {
                type: 0,
                text: "success",
                query: result
            }
            res.send(result);
        }
    });
});


router.post('/questionnairesUser', (req, res) => {
    let query = "select id,name, 1 as done from questionnaire where id in(select questionnaireId  from useranswers where userid = ?) union select id,name, 0 as done from questionnaire where id not in(select questionnaireId  from useranswers where userid = ? )";
    let values = [
        req.body.data,
        req.body.data
    ]
    console.log(req.body.data);
    executeQuery(query, values, (err, result) => {
        if (err)
            console.log(err);
        else {
            let message = {
                type: 0,
                text: "success",
                query: result
            }
            res.send(result);
        }
    });
});

router.post('/questionsFrom', (req, res) => {
    let query = "select * from questions where questionnareId = ?";
    let values = req.body.data;

    executeQuery(query, values, (err, result) => {
        if (err)
            console.log(err);
        else {
            let message = {
                type: 0,
                text: "success",
                query: result
            }
            res.send(result);
        }
    });
});

router.post('/deleteQuestionnaire', (req, res) => {
    let query = "Delete from questionnaire where id = ?";
    let values = req.body.data;
    console.log(values);
    executeQuery(query, values, (err, result) => {
        if (err)
            console.log(err);
        else {
            let query = "Delete from questions where questionnareId = ? "

            executeQuery(query, values, (err, result) => {
                if (err)
                    console.log(err)
                else {
                    let message = {
                        type: 0,
                        text: "success"
                    };
                    res.send(message);
                }
            })
        }
    });
});

router.post('/useranswers', (req, res) => {
    let query = "INSERT into useranswers(questionnaireId,userId,questionNum,answer) values ?;"
    let values = req.body;
    console.log(values);
    executeQuery(query, [values], (err, result) => {
        if (err)
            console.log(err);
        else {
            let message = {
                type: 0,
                text: "success"
            };

            res.send(message);
        }
    })
});

router.post('/updateQuestionnaire', (req, res) => {
    let query = "UPDATE questionnaire set name = ? where id = ?;"
    let values = [
        req.body.name,
        req.body.id
    ]
    console.log(values);
    executeQuery(query, values, (err, result) => {
        if (err)
            console.log(err);
        else {
            let message = {
                type: 0,
                text: "success"
            };
            res.send(message);
        }
    })
});

router.post('/updateQuestions', (req, res) => {
    let values = req.body;
    var finalArray = [];
    let queries = "";
    for (var i = 0; i < values.length; i++) {
        var tempArray = [];
        tempArray.push(values[i].text, values[i].choices, values[i].Id);
        queries += mysql.format("Update questions set text = ?,choices=? where id = ?;", tempArray);
    }
    console.log(finalArray);
    executeQuery(queries, [], (err, result) => {
        if (err)
            console.log(err);
        else {
            let message = {
                type: 0,
                text: "success"
            }
            res.send(message);
        }
    });

});

executeQuery = (query, param, callback) => {
    connection.query(query, param, function (err, result) {
        if (err)
            // execute the callback for a null result and an error.
            callback(err, null);
        else
            // execute the callback on the result
            callback(null, result);
    }.bind(this));
}





module.exports = router;